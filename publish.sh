#!/bin/bash

# Set user
git config user.name "Milk Bath Photoshoot"
git config user.email "milkbathphotography.com@gmail.com"

# Commit and push
git checkout master
git add .
git commit -m "Update"
git push